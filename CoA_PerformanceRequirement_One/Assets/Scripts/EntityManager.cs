using System;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour ,ILog
    {  
        public Entity[] entities ;
        public void Log(object message)
        {
            Debug.Log(message);
        }
        private void Start()
        {
            Log("Names are being registered..");

            for (int i = 0; i < entities.Length; i++)
            {
                Log("Enemy Number "+ i + "'s name is: " + entities[i].EntityName);
            }
        }
    }
}