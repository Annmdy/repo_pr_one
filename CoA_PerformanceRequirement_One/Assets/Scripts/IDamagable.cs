namespace PROne
{
    public interface IDamagable
    {
        void Damage(int damageAmount);
    }
}
