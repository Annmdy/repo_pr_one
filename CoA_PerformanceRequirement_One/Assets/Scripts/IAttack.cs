namespace PROne
{
    public interface IAttack
    {
        void AttackEnemy(Enemy whichEnemy, int damageAmount);
    }
}