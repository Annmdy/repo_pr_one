using System;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack 
    {
        [SerializeField]
        private Enemy enemyEntity;
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attackAmount;
        [SerializeField]
        private bool isAlive = true;
        
        public Enemy EnemyEntity
        {
            get { return enemyEntity; }
            set { enemyEntity = value; }
        }
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }
        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }
        public void AttackEnemy(Enemy whichEnemy, int damageAmount)
        {
            if (whichEnemy.IsAlive == true)
            {
                whichEnemy.Damage(damageAmount);
            }
            else
            {
                Log(" Enemy is already dead!");
            }
        }
        private void Start()
        {
            Log("Name of the Player is: " + EntityName);
            Log("Current health of the Player is: " + CurrentHealth);
            Log("Attack amount of the Player is: " + AttackAmount);
            AttackEnemy(EnemyEntity, AttackAmount);
        }
    }
}
