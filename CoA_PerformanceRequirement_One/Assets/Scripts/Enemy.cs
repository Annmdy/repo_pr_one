using System;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable 
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int  attackAmount;
        [SerializeField]
        private bool isAlive = true;
        
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }
        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }
        public void Damage(int damageAmount)
        {
            if (CurrentHealth > 0)
            {
                CurrentHealth -= damageAmount;
                if (CurrentHealth <= 0)
                { 
                    CurrentHealth = 0;
                    IsAlive = false;
                    
                    Log("Entity: " + EntityName + " was damaged!");
                    Log("Entity's current health is: " + CurrentHealth +", congrats! "+ EntityName + " is dead!");
                    return;
                }
            }
            
            if (CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                IsAlive = false;
                    
                Log("Entity: " + EntityName + " was not damaged!");
                Log("Entity's current health is: " + CurrentHealth +", "+ EntityName + " was already dead!" );
                return;
            }
            Log("Entity: "+ EntityName + " was damaged, current health is : "+ CurrentHealth);
        }
    }
}